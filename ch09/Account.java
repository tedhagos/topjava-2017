

class Account {
  
  String type;
  String name;
  double balance;

  void deposit(double amt) {
    balance += amt;
  }

  void withdraw(double amt) {
    balance -= amt;
  }
}