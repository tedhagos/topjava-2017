class Dog {

  String breed;
  int age;
  String color;  

}

class TestDog {
  
  public static void main(String[] args) {
    Dog d = new Dog();

    d.breed = "Bulldog";
    d.age = 2;
    d.color = "White";
  }
}
