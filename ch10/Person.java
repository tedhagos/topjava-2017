
class Person {

  private String lastname = "";
  private String firstname = "";
  private String email = "";

  void initialize(String mlastname, String mfirstname) {
    lastname = mlastname;
    firstname = mfirstname;
  }

  void setFirstName(String mfirstname) {
    firstname = mfirstname;
  }

  void setLastName(String mlastname) {
    lastname = mlastname;
  }

  void wakeUp(){
    System.out.printf("%s %s is waking up\n", firstname, lastname);
  }

  void sleep(){
    System.out.printf("%s %s is sleeping\n\n", firstname, lastname);
  }

}

class TestPerson {

  public static void main(String[] args) {
    
    Person p1 = new Person();
    //p1.firstname = "John";
    //p1.lastname = "Doe";
    p1.initialize("Doe", "John");
    p1.wakeUp();
    p1.sleep();

    Person p2 = new Person();
    //p2.firstname = "Juan";
    //p2.lastname = "Dela Cruz";
    //p2.initialize("Dela Cruz", "Juan");
    p2.setLastName("Gosling");
    p2.setFirstName("James");
    p2.wakeUp();
    p2.sleep();

    //p1.firstname = "James";

    p1.wakeUp();
    p2.wakeUp();

  }
}