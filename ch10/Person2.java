
class Person {

  String lastname;
  String firstname;

  public Person() { // noarg constructor

  }

  public Person(String last, String first) {
    // some guard conditions here
    lastname = last;
    firstname = first;
  }

  void setName(String last, String first) {

    // some guard conditions here

    lastname = last;
    firstname = first;
  }
  

  void hello() {
    System.out.printf("Hello %s %s\n", firstname, lastname);
  }
}

class TestPerson {


  public static void main(String args) {
    //return 0;
  }

  public static void main(String[] args) {

    Person p1 = new Person("Doe", "John");
    Person p2 = new Person("Doe", "Jane");
    p1.hello();
    p2.hello();

    Person p3 = new Person();
    p3.hello();
  
  }

}