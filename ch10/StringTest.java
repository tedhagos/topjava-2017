

class StringTest {

  public static void main(String[] args) {
    
    String s1 = "Hello";
    
    String s2 = new String("Hello");
    // int a = 10;


    // 1, 2 (byte, short, long, ints) literals
    // 0.0, 1.1 (double, float) literals
    // true, false (boolean literals)
    // 'a', \u0000, \uFFFF, 1, 2 , 65535 (unicode literals)
    // "This is a String literal"

    String s3 = "Hello";
    String s4 = " World";
    String s5 = s3.concat(s4);

    System.out.println(s5);

    System.out.printf("s5 is %d\n", s5.length());

    System.out.println(s5.startsWith("he"));

  }
}