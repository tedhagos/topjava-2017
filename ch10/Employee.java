

class Employee extends Person {

  public Employee(String last, String first) {
    lastname = last;
    firstname = first;
  }

  public static void main(String[] args) {
    
    Employee e = new Employee();
    e.setName("Doe", "Jane");
    e.hello();

  }

}

