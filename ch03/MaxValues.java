
import java.lang.Math;

class MaxValues {

  static double upperBound(int bitsize) {
    return (Math.pow(2,(bitsize - 1)) + 1);
  }
  
  static double lowerBound(int bitsize) {
    return (1) * Math.pow(2,(bitsize-1));
  }

  public static void main (String [] args){
    System.out.println("Byte maxValue =  " + upperBound(8));
    System.out.println("Byte minValue =  " + lowerBound(8));
  }

}
