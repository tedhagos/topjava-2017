

class gcf {

  public static void main(String[] args) {
    
    // check if there are at least 2 numbers from
    // the args
    
    if (args.length < 2) {
      System.out.println("Usage is `java gcf int1 int2`");
      System.exit(0);
    }

    // get the numbers, from the env via args

    int firstNumber = Integer.parseInt(args[0]);
    int secondNumber = Integer.parseInt(args[1]);

    // find the big and small number

    int bigNumber = 0;
    int smallNumber = 0;

    if(firstNumber > secondNumber) {
      bigNumber = firstNumber;
      smallNumber = secondNumber;
    }
    else {
      bigNumber = secondNumber;
      smallNumber = firstNumber;      
    }

    // System.out.printf("Big number is %d\n", bigNumber);
    // System.out.printf("Small number is %d", smallNumber);

    int rem = 1;
    int counter = 1 ;

    while((rem = bigNumber % smallNumber) != 0) {
      bigNumber = smallNumber;
      smallNumber = rem;
      System.out.printf("counter %d\n",counter++);        
    }
    
    System.out.printf("GCF is %d", smallNumber);

  }

}