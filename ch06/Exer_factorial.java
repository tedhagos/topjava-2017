
// Exer
// Compute for the factorial of a positive integer

class Exer_factorial {

  public static void main(String[] args) {
    
    if (args.length == 0) {
      System.out.println("Usage is `java Exer_factorial int`");
      System.exit(0);
    }

    int number = Integer.parseInt(args[0]);
    long fact = 1;
    for (int j = 1; j <= number; j++) {
      fact *= j;
    }
    System.out.printf("%d! = %d", number,fact);
  }

}