

class Exer1 {

  public static void main(String[] args) {
    
    for (int count = 0; count < 100; count++) {
      
      if((count % 2 ) == 0) {
        //System.out.println("Counter " + count + " is even");
        System.out.printf("Counter %d is even\n", count);
      }    
      else {
        System.out.printf("Counter %d is odd\n", count);
        //System.out.println("Counter " + count + " is odd");
      }

    }
  }
}