// Exer
// Find the sum of all even integers from 0 .. 100
// Find the sum of all even integers from 0 .. 100

class Exer_Sum2 {
  
  public static void main(String[] args) {

    int sumEven = 0;
    int sumOdd = 0;

    for (int i=0; i<=100;i++) {
      if ((i %2) == 0) {
        // EVEN
        sumEven += i;
      }
      else {
        // ODD
        sumOdd += i;
      }
    }
    
    System.out.printf("Sum of even numbers from 0..100 = %d\n", sumEven);
    System.out.printf("Sum of odd numbers from 0..100 = %d\n", sumOdd);

  }

}