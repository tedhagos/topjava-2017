/*
  
  A program that computes for 
  greatest common factor. This code
  follows Euclid's algorithm

*/

class Exer3 {

  static int bigNumber = 0;
  static int smallNumber = 0;

  static void checkForArgs(String[] args) {  
    
    // This checking is not very robust, it only checks
    // if there are at least 2 arguments, there are 
    // a lot of scenarios we are not testing for e.g.
    //    - more then 2 args
    //    - if the args are not int

    if (args.length < 2) {
      System.out.println("Usage is java Exer2 int int");
      System.exit(0);  // Shutdown gracefully
    }    
  }

  public static void main(String[] args) {

    checkForArgs(args);
    // at this point, I know that there are sufficient arguments
    // for my code

    // Now set the big and small number
    int firstNumber = Integer.parseInt(args[0]);
    int secondNumber = Integer.parseInt(args[1]);

    // find out which is the bigger number

    if (firstNumber > secondNumber) {
      bigNumber = firstNumber;
      smallNumber = secondNumber;
    } 
    else {
      bigNumber = secondNumber;
      smallNumber = secondNumber;
    }

    // Now, we can move on to Euclid's algo

    int rem = 1;

    while (rem != 0) {
      rem = bigNumber % smallNumber;
      if(rem == 0) {
        System.out.printf("GCF = %d", smallNumber);
      }
      else {
        bigNumber = smallNumber;
        smallNumber = rem;
      }
    }

  }
}