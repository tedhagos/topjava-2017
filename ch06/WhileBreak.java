

class WhileBreak {

  public static void main(String[] args) {
    
    int count = 0;

    while(true) {

      count++;
      System.out.println(count);

      if(count >= 10) {
        break;
      }

      // Unreachable code
      //System.out.println("After the break");

    }

  }
}