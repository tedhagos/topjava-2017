import java.util.*;

    class Elif {
      public static void main(String[] args) {
        
        /*
        What the next 4 lines does:
        1. Create a calendar object
        2. Create a date object, which holds
           the current date
        3. Get the current day of the week which
           is basically an int 1..7   
        */
        Calendar c = Calendar.getInstance();
        Date d = new Date();
        c.setTime(d);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        // Next, print out the friendly name for the
        // current day of week

        if (dayOfWeek == 1) {
          System.out.println("Sunday");
        }
        else if (dayOfWeek == 2) {
          System.out.println("Monday");
        }
        else if (dayOfWeek == 3) {
          System.out.println("Tuesday");
        }
        else if (dayOfWeek == 4) {
          System.out.println("Wednesday");
        }
        else if (dayOfWeek == 5) {
          System.out.println("Thursday");
        }
        else if (dayOfWeek == 6) {
          System.out.println("Friday");
        }
        else if (dayOfWeek == 7) {
          System.out.println("Saturday");
        }

      }
    }