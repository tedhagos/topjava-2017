
// Exer
// Find the sum of all integers from 0 .. 100

class Exer_Sum1 {
  
  public static void main(String[] args) {

    int sum = 0;
    
    for (int i=0; i<=100; i++) {
      sum += i;
    }
    System.out.printf("Sum of numbers from 0..100 = %d", sum);
  }

}